window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();

        for (let conference of data.conferences) {
            const option = document.createElement('option');
            option.value = conference.href;
            option.innerHTML = conference.name;
            selectTag.appendChild(option);
        }
        selectTag.classList.remove("d-none")
        const loadTag = document.getElementById('loading-conference-spinner')
        loadTag.classList.add("d-none")
    }

    const attendeeForm = document.getElementById('create-attendee-form');
    attendeeForm.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(attendeeForm);
        const json = JSON.stringify(Object.fromEntries(formData));
        const attendeeUrl = 'http://localhost:8001/api/attendees/'
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(attendeeUrl, fetchConfig);
        console.log(response)
        if (response.ok) {
            const newConference = await response.json(response)
            const successAlert = document.getElementById('success-message');
            successAlert.classList.remove("d-none");
            attendeeForm.classList.add("d-none");
            console.log(newConference)
        }
    })
});
