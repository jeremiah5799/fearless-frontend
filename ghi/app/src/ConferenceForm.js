import React, { useEffect, useState } from 'react';

function ConferenceForm() {
    const [locations, setLocations] = useState([]);



    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/'

        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()

        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new conference</h1>
              <form id="create-conference-form">

                <div className="form-floating mb-3">
                  <input placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                  <label htmlFor="name">Name</label>
                </div>

                <div className="form-floating mb-3">
                  <input placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" />
                  <label htmlFor="starts">Start Date</label>
                </div>

                <div className="form-floating mb-3">
                  <input placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" />
                  <label htmlFor="ends">End Date</label>
                </div>

                <div className="mb-3">
                  <label htmlFor="description" className="form-label">Description</label>
                  <textarea required type="text" className="form-control" name="description" id="description"></textarea>
                </div>

                <div className="form-floating mb-3">
                  <input placeholder="max_presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
                  <label htmlFor="max_presentations">Max Presentations</label>
                </div>

                <div className="form-floating mb-3">
                  <input placeholder="max_attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
                  <label htmlFor="max_attendees">Max Attendeess</label>
                </div>

                <div className="mb-3">
                  <select required name="location" id="location" className="form-select">
                  <option value="">Location</option>
                  </select>
                </div>

                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
}

export default ConferenceForm;
